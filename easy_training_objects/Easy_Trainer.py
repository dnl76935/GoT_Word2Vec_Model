# Import Dependencies

from __future__ import absolute_import, division, \
    print_function  # the bridge between python 2 and python 3 I think?
import codecs  # for word encoding
import glob  # for regex
import multiprocessing  # for concurrency
import os  # dealing with operating systems, like reading a file
import pprint  # pretty printing, human readable
import re  # regular expressions
import nltk  # Natural Language Toolkit
import gensim.models.word2vec as w2v  # word 2 vec
import gensim  # gensim
import sklearn.manifold  # dimensionality reduction
import numpy as np  # math
import matplotlib.pyplot as plt  # plotting
import pandas as pd  # parse pandas as pd
import seaborn as sns  # visualization
import time  # for diagnostics

class Easy_Trainer:

    def __init__(self, sentences=None, saveName=""):
        self.sentences = sentences
        self.saveName = saveName


    @staticmethod
    def loadModel(modelName):
        return gensim.models.Word2Vec.load("trained/" + modelName)

    def train(self):

        # If there is nothing to train on, exit
        if self.sentences == "" or self.sentences == []:
            print("There are no sentences (training data) provided")
            return None

        # Clean data
        nltk.download('punkt')  # pretrained tokenizer
        nltk.download('stopwords')  # words like and, the, an, a, of (words that don't have a lot of semantic meaning)

        # Dimensionality of the resulting word vectors
        # More dimensions means more time to train, but more accurate
        num_features = 300;

        # Minimum word count threshold
        min_word_count = 3;

        # Number of threads to run in parallel
        # More workers, faster we train
        num_workers = multiprocessing.cpu_count()

        # Context window length
        context_size = 7

        # Downsample setting for frequent words
        downsampling = 1e-3

        # Seed for the RNG, to make the results more reproducible
        # Makes it deterministic, good for debugging
        seed = 1

        print("Training model......")
        w2v_start_time = time.time()
        # Build word2vec model
        self.model = w2v.Word2Vec(
            sentences=self.sentences,
            sg=1,
            seed=seed,
            workers=num_workers,
            size=num_features,
            min_count=min_word_count,
            window=context_size,
            sample=downsampling
        )
        print("Trained model in %s seconds!" % (time.time() - w2v_start_time))

        # Make trained directory if nonexistant
        if not os.path.exists("trained"):
            os.makedirs("trained")

        # Save it
        if self.saveName == "":
            currTime = time.time()
            self.model.save(os.path.join("trained", currTime + ".w2v"))
            print("Model saved under trained/" + currTime + ".w2v")
        else:
            self.model.save(os.path.join("trained", self.saveName + ".w2v"))
            print("Model saved under trained/" + self.saveName + ".w2v")


    # Each element in txtArr should be a string
    def setSentencesFromTextFiles(self, txtArr):

        corpus_raw = u""

        for txt in txtArr:
            corpus_raw += txt

        # pretrained tokenizer ( turns words into tokens )
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

        # tokenizes the corpus into sentences
        raw_sentences = tokenizer.tokenize(corpus_raw)

        # sentence where each word is tokenized
        self.sentences = []
        for raw_sentence in raw_sentences:
            if len(raw_sentence) > 0:
                self.sentences.append(self.sentence_to_wordlist(raw_sentence))


    # convert into a list of words
    # remove unnecessary characters
    # split into words with no hyphens, etc
    def sentence_to_wordlist(self, raw):
        clean = re.sub("[^a-zA-Z]", " ", raw)
        words = clean.split()
        return words