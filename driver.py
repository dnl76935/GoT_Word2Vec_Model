from easy_training_objects.Easy_Trainer import Easy_Trainer
import pandas as pd
import numpy as np
import time
import scipy
import math
# import glob
# import nltk
# import codecs
# import re
# # Clean data
# nltk.download('punkt')  # pretrained tokenizer
# nltk.download('stopwords')  # words like and, the, an, a, of (words that don't have a lot of semantic meaning)
#
# # Get the book names, matching txt file
# book_filenames = sorted(glob.glob('data/*.txt'))
#
# # Raw corpus
# corpus_raw = u""  # Use 'u' here because it is a unicode string
#
# # After this for loop, corpus_raw will contain all of the raw data from the books
# for book_filename in book_filenames:
#     print("Reading '{0}'...".format(book_filename))
#     # Using codecs library here
#     # This converts the book_filename into utf-8 format
#     with codecs.open(book_filename, "r", "utf-8") as book_file:
#         corpus_raw += book_file.read()  # add all the books to the corpus
#
#     print("Corpus is now {0} characters long".format(len(corpus_raw)))
#     print()
#
# # pretrained tokenizer ( turns words into tokens )
# tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
#
# # tokenizes the corpus into sentences
# raw_sentences = tokenizer.tokenize(corpus_raw)
#
#
# # convert into a list of words
# # remove unnecessary characters
# # split into words with no hyphens, etc
# def sentence_to_wordlist(raw):
#     clean = re.sub("[^a-zA-Z]", " ", raw)
#     words = clean.split()
#     return words
#
#
# # sentence where each word is tokenized
# sentences = []
# for raw_sentence in raw_sentences:
#     if len(raw_sentence) > 0:
#         sentences.append(sentence_to_wordlist(raw_sentence))

# Load model
model = Easy_Trainer.loadModel("thrones2vec.w2v")

# Get all the vectors
vectors = []
for word in model.wv.vocab:
    vectors.append(model.wv[word])

def dist(u, v):

    # Only do this if the lengths are equal
    if(len(u) == len(v)):
        # iterate through all and use Euclidean distance definition
        diff = np.array(u) - np.array(v)
        distance = np.dot(diff, diff)

    else:
        # Error
        print("Vectors have mismatched lengths of " + len(u) + " and " + len(v))
        return 0

    # return
    return distance

numVectors = len(vectors) # number of vectors

# init
distances = [];
distanceRow = [];

t = time.time()

# for i in range(numVectors):
#     distanceRow = []
#     print(i)
#     print("Elapsed Time: %s seconds" % format(time.time() - t))
#     t = time.time()
#     for j in range(numVectors):
#         distanceRow.append(dist(vectors[i], vectors[j]))
#     distances.append(distanceRow)

distances = np.zeros((numVectors, numVectors))

from multiprocessing import Process

print(numVectors)

def findDistances(slice):
    for i in range(slice[0], slice[1]):
        print(i)
        t = time.time()
        print("Elapsed Time: %s seconds" % format(time.time() - t))
        for j in range(i):
            distances[i][j] = dist(vectors[i], vectors[j])

# p1 = Process(target = findDistances, args = ([0, 4000], ))
# p2 = Process(target = findDistances, args = ([4001, 8000], ))
# p3 = Process(target = findDistances, args = ([8001, 12000], ))
# p4 = Process(target = findDistances, args = ([12001, numVectors], ))
#
startTime = time.time()
#
# # print("p1")
# p1.start()
# # print("p2")
# p2.start()
# # print("p3")
# p3.start()
# # print("p4")
# p4.start()
#
# p1.join()
# p2.join()
# p3.join()
# p4.join()

findDistances([0, 4000])

for i in range(100):
    for j in range(i):
        print(distances[i][j])

# for i in range(numVectors):
#     print(i)
#     t = time.time()
#     print("Elapsed Time: %s seconds" % format(time.time() - t))
#     for j in range(i):
#         distances[i][j] = dist(vectors[i], vectors[j])

print("Took %s seconds" % format(time.time() - startTime))

np.savetxt('data/distances.txt', distances, fmt = '%f')

# distances[0][0] = 5
# distances[1][1] = 6
#
# print(distances[0][0])
# print(distances[1][1])

# for v in vectors:
#     for u in vectors:
