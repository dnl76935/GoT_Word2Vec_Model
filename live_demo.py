# TODO Goal: Create word vectors from a Game of Thrones dataset and analyze them to see semantic similarity

# Step 0 -- Import Dependencies
from __future__ import absolute_import, division, print_function # the bridge between python 2 and python 3 I think?
import codecs # for word encoding
import glob # for regex
import multiprocessing # for concurrency
import os # dealing with operating systems, like reading a file
import pprint # pretty printing, human readable
import re # regular expressions
import nltk # Natural Language Toolkit
import gensim.models.word2vec as w2v # word 2 vec
import gensim # gensim
import sklearn.manifold # dimensionality reduction
import numpy as np # math
import matplotlib.pyplot as plt # plotting
import pandas as pd # parse pandas as pd
import seaborn as sns # visualization
import time # for diagnostics


# If there is a model already trained, load it
# Else, train a new model
if os.path.isfile('trained/thrones2vec.w2v'):
    thrones2vec = gensim.models.Word2Vec.load('trained/thrones2vec.w2v')  # pretrained
else:

    # Step 1 -- Process Data

    # Clean data
    nltk.download('punkt') # pretrained tokenizer
    nltk.download('stopwords') # words like and, the, an, a, of (words that don't have a lot of semantic meaning)

    # Get the book names, matching txt file
    book_filenames = sorted(glob.glob('data/*.txt'))

    # Raw corpus
    corpus_raw = u"" # Use 'u' here because it is a unicode string

    # After this for loop, corpus_raw will contain all of the raw data from the books
    for book_filename in book_filenames:
        print("Reading '{0}'..." . format(book_filename))
        # Using codecs library here
        # This converts the book_filename into utf-8 format
        with codecs.open(book_filename, "r", "utf-8") as book_file:
            corpus_raw += book_file.read() # add all the books to the corpus

        print("Corpus is now {0} characters long" . format(len(corpus_raw)))
        print()

    # pretrained tokenizer ( turns words into tokens )
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

    # tokenizes the corpus into sentences
    raw_sentences = tokenizer.tokenize(corpus_raw)

    # convert into a list of words
    # remove unnecessary characters
    # split into words with no hyphens, etc
    def sentence_to_wordlist(raw):
        clean = re.sub("[^a-zA-Z]", " ", raw)
        words = clean.split()
        return words

    # sentence where each word is tokenized
    sentences = []
    for raw_sentence in raw_sentences:
        if len(raw_sentence) > 0:
            sentences.append(sentence_to_wordlist(raw_sentence))

    # print how many tokens we have
    token_count = sum([len(sentence) for sentence in sentences])
    print("The book corpus contains {0:,} tokens" . format(token_count))


    # Step 3 -- Build Model

    # Once we have vectors, there are 3 main tasks they help with
        # 1: Distance
        # 2: Similarity
        # 3: Ranking

    # Dimensionality of the resulting word vectors
        # The more dimensions, the more computationally expensive it is to train, but also more accurate
    num_features = 300;

    # Minimum word count threshold
    min_word_count = 3;

    # Number of threads to run in parallel
        # More workers, faster we train
    num_workers = multiprocessing.cpu_count()

    # Context window length
    context_size = 7

    # Downsample setting for frequent words
        # TODO: look up what this means exactly, I don't get it
    downsampling = 1e-3

    # Seed for the RNG, to make the results reproducible
        # Makes it deterministic, good for debugging
    seed = 1

    print("Training model......")
    w2v_start_time = time.time()
    # Build word2vec model
    thrones2vec = w2v.Word2Vec(
        sentences = sentences,
        sg = 1,
        seed = seed,
        workers = num_workers,
        size = num_features,
        min_count = min_word_count,
        window = context_size,
        sample = downsampling
    )
    print("Trained model in %s seconds!" % (time.time() - w2v_start_time))

    # Save to file to use later
    if not os.path.exists("trained"):
        os.makedirs("trained")

    # Save the model
    thrones2vec.save(os.path.join("trained", "thrones2vec.w2v"))
    print("Model saved")

# If there is already a word vector 2d matrix, load it
# Else, train a new one using tnse and save it
if os.path.isfile('trained/all_word_vectors_matrix_2d.txt'):
    all_word_vectors_matrix_2d = np.loadtxt('trained/all_word_vectors_matrix_2d.txt', dtype=float)  # pretrained
else:
    # Tutorial video -- How To Visualize a Dataset Easily (by Siraj Raval)
    tsne = sklearn.manifold.TSNE(n_components = 2, random_state = 0)

    # ??
    all_word_vectors_matrix = thrones2vec.wv.syn0

    # Train tsne
    # Put word vectors into 2d matrix
    print("Training tsne......")
    tsne_start_time = time.time()

    all_word_vectors_matrix_2d = tsne.fit_transform(all_word_vectors_matrix)

    print("Trained tnse in %s seconds!" % (time.time() - tsne_start_time))

    # Save the matrix
    np.savetxt('trained/all_word_vectors_matrix_2d.txt', all_word_vectors_matrix_2d, fmt = '%f')

# Plot the big picture
points = pd.DataFrame(
    [
        (word, coords[0], coords[1])
        for word, coords in [
            (word, all_word_vectors_matrix_2d[thrones2vec.wv.vocab[word].index])
            for word in thrones2vec.wv.vocab
        ]
    ],
    columns = ["word", "x", "y"]
)

# Check out the head of the points
points.head(10)

# Context?
sns.set_context("poster")

# Function to zoom in on interesting places
def plot_region(x_bounds, y_bounds):
    slice = points.loc[
        (points['x'] >= x_bounds[0]) &
        (points['x'] <= x_bounds[1]) &
        (points['y'] >= y_bounds[0]) &
        (points['y'] <= y_bounds[1])
    ]

    if not slice.empty:
        ax = slice.plot.scatter("x", "y", s = 35, figsize = (10, 8))

        for i, point in slice.iterrows():
            ax.text(point.x + 0.005, point.y + 0.005, point.word, fontsize = 5)

        plt.show()

def plot_around_vector(vector, window):
    x = vector[0]
    y = vector[1]

    x_bounds = (x - window, x + window)
    y_bounds = (y - window, y + window)

    plot_region(x_bounds, y_bounds)

def plot_around_word(word, window):
    x = points.loc[points.word == word]['x'].values[0]
    y = points.loc[points.word == word]['y'].values[0]

    plot_around_vector((x, y), window)

# Scatter plot the 2d vectors
points.plot.scatter("x", "y", s = 10, figsize = (20, 12))
plt.show()

# Plot a specific region
plot_region(x_bounds = (4.0, 4.2), y_bounds = (-50, 20))

# Plot a specific region around a word
interestingWord = 'apples'
window = 1
plot_around_word(interestingWord, window)